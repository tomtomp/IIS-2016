<?php

namespace App\Presenters;

use App\Forms\SignFormFactory;
use App\Forms\TeaserFormFactory;
use App\Model\DuplicateNameException;
use App\Model\NoSpareAdmin;
use App\Model\UserManager;
use Nette;
use App\Services;
use Tracy\Bar;
use Tracy\Debugger;


/**
 * Class UserPresenter
 * @package App\Presenters
 */
class UserPresenter extends BasePresenter
{

    /**
     * @var UserManager
     */
    private $userManager;

    /**
     * @var Nette\Database\Table\ActiveRow
     */
    private $userRow;

    /**
     * @var SignFormFactory @inject
     */
    public $singFromFactory;


    public function __construct(UserManager $userManager)
    {
        $this->userManager = $userManager;
    }

    public function startup()
    {
        parent::startup();
        $id = $this->getParameter('id');
        if ($id) {
            $this->userRow = $this->userManager->getUsers()->get($id);
        }
    }


    public function actionView($id)
    {
        if (!$this->userRow) {
            $this->flashMessage('Přihlaste se prosím.', 'danger');
            $this->redirect('Sign:in', ['backlink' => $this->storeRequest()]);
        }
        if ($this->userRow->id != $this->user->id && !$this->user->isAllowed('User:manage')) {
            $this->flashMessage('Nedostatečné oprávění.', 'danger');
            $this->redirect('Catalog:');
        }

    }

    public function actionEdit($id)
    {
        if (!$this->userRow) {
            $this->flashMessage('Neplatné id uživatele.', 'danger');
            $this->redirect('Catalog:');
        }
        if ($this->userRow->id != $this->user->id && !$this->user->isAllowed('User:manage')) {
            $this->flashMessage('Nedostatečné oprávění.', 'danger');
            $this->redirect('Catalog:');
        }
    }

    public function actionDelete($id)
    {
        if (!$this->userRow) {
            $this->flashMessage('Neplatné id uživatele.', 'danger');
            $this->redirect('Catalog:');
        }
        if ($this->userRow->id != $this->user->id && !$this->user->isAllowed('User:manage')) {
            $this->flashMessage('Nedostatečné oprávění.', 'danger');
            $this->redirect('Catalog:');
        }
        $this->userManager->deleteUser($id);
        $this->flashMessage('Uživatel smazán.', 'danger');
        $this->redirect('Catalog:');
    }


    public function renderView($id)
    {
        $this->template->userData = Nette\Utils\ArrayHash::from($this->userRow->toArray());
    }

    public function renderEdit($id)
    {
        $this->template->userData = Nette\Utils\ArrayHash::from($this->userRow->toArray());
    }

    protected function createComponentEditForm()
    {
        $form = $this->singFromFactory->createEdit($this->user->isAllowed('User:manage'));
        $form->setDefaults($this->userRow->toArray());
        $form->onSuccess[] = ($this->user->isAllowed('User:manage')) ? [$this, 'saveUserLikeABoss'] : [$this, 'saveUser'];
        $form->addSubmit('save', 'Uložit');
        return $form;
    }

    public function saveUser(Nette\Forms\Form $form, $values)
    {
        if (!$this->userRow) {
            $this->flashMessage('Neplatné id uživatele.', 'danger');
            $this->redirect('Catalog:');
        }

        if ($this->userRow->id != $this->user->id) {
            $this->flashMessage('Nedostatečné oprávění.', 'danger');
            $this->redirect('Catalog:');
        }
        try {
            if ($values['password']) {
                $this->userManager->authenticate([$values['email'], $values['oldpassword']]);
            }
            unset($values['oldpassword']);
            unset($values['password2']);
            $this->userManager->editUser($this->userRow->id, $values);
        } catch (Nette\Security\AuthenticationException $e) {
            $this->flashMessage('Původní heslo se neshoduje.', 'danger');
            return;
        } catch (DuplicateNameException $e) {
            $this->flashMessage('E-mail nebo rodné číslo již evidujeme.', 'danger');
            return;
        }
        $this->flashMessage('Uživatel uložen.');
        $this->redirect('User:view', ['id' => $this->userRow->id]);
    }

    public function saveUserLikeABoss(Nette\Forms\Form $form, $values)
    {
        if (!$this->userRow) {
            $this->flashMessage('Neplatné id uživatele.', 'danger');
            $this->redirect('Catalog:');
        }
        try {
            unset($values['password2']);
            $this->userManager->editUser($this->userRow->id, $values);
        } catch (DuplicateNameException $e) {
            $this->flashMessage('E-mail nebo rodné číslo již evidujeme.', 'danger');
            return;
        }catch (NoSpareAdmin $e) {
            $this->flashMessage('V systému je nedostatek administrátorů.', 'danger');
            return;
        }

        $this->flashMessage('Uživatel uložen.');
        $this->redirect('User:view', ['id' => $this->userRow->id]);
    }

}
