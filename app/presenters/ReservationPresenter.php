<?php

namespace App\Presenters;

use App\Forms\ReservationFormFactory;
use App\Model\UserManager;
use Nette;
use App\Services;
use Tracy\Debugger;
use IPub\VisualPaginator\Components as VisualPaginator;


/**
 * Base presenter for all application presenters.
 */
class ReservationPresenter extends BasePresenter
{

    /**
     * @var Services\TeaserService
     */
    private $teaserService;

    /**
     * @var UserManager
     */
    private $userManager;

    /**
     * @var Nette\Database\Table\IRow
     */
    private $loan;

    /**
     * @var ReservationFormFactory
     */
    private $reservationFormFactory;


    public function __construct(Services\TeaserService $teaserService, ReservationFormFactory $reservationFormFactory, UserManager $userManager)
    {
        $this->teaserService = $teaserService;
        $this->reservationFormFactory = $reservationFormFactory;
        $this->userManager = $userManager;
    }


    public function actionEdit($id)
    {
        if (!$id || !($this->loan = $this->teaserService->getLoans()->get($id))) {
            $this->flashMessage('Neplatné id rezervace', 'danger');
            $this->redirect('Catalog:');
        }
        if (!$this->user->isAllowed('Reservation:manage')) {
            $this->flashMessage('Nemáte oprávnění spravovat rezervace', 'danger');
            $this->redirect('Catalog:');
        }
    }

    public function actionAdd($teaserId = null)
    {
        if (!$this->user->isAllowed('Reservation:manage')) {
            $this->flashMessage('Nemáte oprávnění spravovat rezervace', 'danger');
            $this->redirect('Catalog:');
        }
    }

    public function actionDelete($id)
    {
        if (!$id || !($this->loan = $this->teaserService->getLoans()->get($id))) {
            $this->flashMessage('Neplatné id rezervace', 'danger');
            $this->redirect('Catalog:');
        }
        if (!$this->user->isAllowed('Reservation:manage')) {
            $this->flashMessage('Nemáte oprávnění spravovat rezervace', 'danger');
            $this->redirect('Reservations:all');
        }
        try {
            $this->teaserService->deleteLoan($id);
            $this->flashMessage('Rezervace byla smazána', 'success');
        } catch (Services\TeaserException $e) {
            $this->flashMessage($e->getMessage(), 'danger');
            $this->redirect('Reservations:all');
        }
        $this->flashMessage('Rezervace smazána');
        $this->redirect('Reservations:all');
    }

    protected function createComponentEditForm()
    {
        $loan = $this->loan;
        $defaults = $loan->toArray();
        $users = [null => '---'];
        foreach ($this->userManager->getUsers()->order('surname ASC')->fetchPairs('id') as $userId => $user) {
            $users[$userId] = sprintf('%s %s(%d)', $user->name, $user->surname, $userId);
        };
        $teasers =  ['' => '---'];
        $teasers +=  (array) $this->teaserService->getTeasers()->order('name ASC')->fetchPairs('id', 'name');
        $form = $this->reservationFormFactory->createEdit($users, $teasers);
        $form->addSubmit('save', 'Uložit');
        if ($loan->l_from) {
            $defaults['l_from'] = $loan->l_from->format('j.n.Y');
        }
        if ($loan->l_to && $loan->l_to->format('j.n.Y') != '1.1.1970') {
            $defaults['l_to'] = $loan->l_to->format('j.n.Y');
        } else {
            $defaults['l_to'] = '';
        }
        $form->setDefaults($defaults);
        $form->onSuccess[] = [$this, 'saveReservation'];
        return $form;
    }

    protected function createComponentAddForm()
    {
        $teaserId = $this->getParameter('teaserId');
        $users = ['' => '---'];
        foreach ($this->userManager->getUsers()->order('surname ASC')->fetchPairs('id') as $userId => $user) {
            $users[$userId] = sprintf('%s %s(%d)', $user->name, $user->surname, $userId);
        };
        $teasers =  ['' => '---'];
        $teasers +=  (array) $this->teaserService->getTeasers()->order('name ASC')->fetchPairs('id', 'name');
        $form = $this->reservationFormFactory->createEdit($users, $teasers);
        $form->addSubmit('add', 'Uložit');
        if ($teaserId) {
            $form->setDefaults(['pp_teaser_id' => $teaserId]);
        }
        $form->onSuccess[] = [$this, 'addReservation'];
        return $form;
    }

    public function saveReservation(Nette\Forms\Form $form, $values)
    {
        try {
            $this->teaserService->saveLoan($this->loan->id, $values);
            $this->flashMessage('Rezervace uložena');
        } catch (Services\TeaserException $e) {
            $this->flashMessage($e->getMessage(), 'danger');
        }
        $this->redirect('Reservations:all');
    }

    public function addReservation(Nette\Forms\Form $form, $values)
    {
        try {
            $this->teaserService->addLoan($values);
            $this->flashMessage('Rezervace uložena.');
        } catch (Services\TeaserException $e) {
            $this->flashMessage($e->getMessage(), 'danger');
        }
        $this->redirect('Reservations:all');
    }

}
