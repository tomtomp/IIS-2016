<?php

namespace App\Forms;

use Nette;
use Nette\Application\UI\Form;
use App\Model;
use Tracy\Debugger;


class SignFormFactory
{
    use Nette\SmartObject;

    const PASSWORD_MIN_LENGTH = 7;

    /** @var FormFactory */
    private $factory;

    /**
     * @var Nette\Security\Permission
     */
    public $permission;


    public function __construct(FormFactory $factory, Nette\Security\Permission $permission)
    {
        $this->factory = $factory;
        $this->permission = $permission;
    }


    /**
     * @return Form
     */
    public function createUp()
    {
        $form = $this->factory->create();

        $form->addText('name', 'Jméno: *')
            ->setRequired('Prosím zadejte své jméno.');

        $form->addText('surname', 'Příjmení: *')
            ->setRequired('Prosím zadejte své příjmení.');

        $form->addText('rc', 'Rodné číslo: *')
            ->setRequired('Prosím zadejte své rodné číslo.')
            ->setNullable()
            ->addRule(Form::INTEGER, 'Zadejte prosím celé číslo');

        $form->addText('city', 'Město:');

        $form->addText('street', 'Ulice:');

        $form->addText('country', 'Země:');

        $form->addText('zip', 'PSČ:');

        $form->addText('email', 'E-mailová adresa *:')
            ->setRequired('Prosím zadejte svůj e-mail.')
            ->addRule($form::EMAIL, 'E-mail není plantný');

        $form->addPassword('password', 'Heslo:')
            ->setOption('description', sprintf('nejméně %d znaků.', self::PASSWORD_MIN_LENGTH))
            ->setRequired('Prosím zadejte heslo.')
            ->addRule($form::MIN_LENGTH, sprintf('Prosím zadejte nejméně %d znaků.', self::PASSWORD_MIN_LENGTH), self::PASSWORD_MIN_LENGTH);

        $form->addPassword('password2', 'Zadejte heslo znovu:')
            ->setRequired('Prosím zopakujte heslo.')
            ->addRule($form::EQUAL, 'Hesla se neshodují.', $form['password']);


        return $form;
    }

    public function createIn()
    {

        $form = $this->factory->create();
        $form->addText('email', 'E-mail:')
            ->setRequired('Prosím zadejte e-mail.')
            ->addRule($form::EMAIL);

        $form->addPassword('password', 'Heslo:')
            ->setRequired('Prosím zadejte heslo.');

        $form->addCheckbox('remember', 'Zůstat přihlášen');

        $form->addSubmit('send', 'Přihlásit');


        return $form;

    }

    public function createForgot()
    {

        $form = $this->factory->create();
        $form->addText('email', 'E-mail:')
            ->setRequired('Prosím zadejte e-mail.')
            ->addRule($form::EMAIL);

        $form->addSubmit('forget', 'Obnovit heslo');


        return $form;

    }

    public function createRescue()
    {

        $form = $this->factory->create();

        $form->addPassword('password', 'Heslo:')
            ->setOption('description', sprintf('nejméně %d znaků.', self::PASSWORD_MIN_LENGTH))
            ->setRequired('Prosím zadejte heslo.')
            ->addRule($form::MIN_LENGTH, sprintf('Prosím zadejte nejméně %d znaků.', self::PASSWORD_MIN_LENGTH), self::PASSWORD_MIN_LENGTH);

        $form->addPassword('password2', 'Zadejte heslo znovu:')
            ->setRequired('Prosím zopakujte heslo.')
            ->addRule($form::EQUAL, 'Hesla se neshodují.', $form['password']);


        return $form;

    }

    public function createEdit($admin = false)
    {
        $form = $this->factory->create();

        $form->addText('name', 'Jméno: *')
            ->setRequired('Prosím zadejte své jméno.');

        $form->addText('surname', 'Příjmení: *')
            ->setRequired('Prosím zadejte své příjmení.');

        $form->addText('rc', 'Rodné číslo: *')
            ->setRequired('Prosím zadejte své rodné číslo.')
            ->setNullable()
            ->addRule(Form::INTEGER, 'Zadejte prosím celé číslo');

        $form->addText('city', 'Město:');

        $form->addText('street', 'Ulice:');

        $form->addText('country', 'Země:');

        $form->addText('zip', 'PSČ:');

        $form->addText('email', 'E-mailová adresa: *')
            ->setRequired('Prosím zadejte svůj e-mail.')
            ->addRule($form::EMAIL, 'E-mail není plantný');


        $form->addPassword('password', 'Heslo:')
            ->setRequired(false)
            ->setOption('description', sprintf('nejméně %d znaků', self::PASSWORD_MIN_LENGTH))
            ->addRule($form::MIN_LENGTH, sprintf('Prosím zadejte nejméně %d znaků.', self::PASSWORD_MIN_LENGTH), self::PASSWORD_MIN_LENGTH);

        $form->addPassword('password2', 'Zadejte heslo znovu:')
            ->setRequired(false)
            ->addConditionOn($form['password'], $form::FILLED, true)
            ->setRequired('Hesla se neshodují.')
            ->addRule($form::EQUAL, 'Hesla se neshodují.', $form['password']);

        if (!$admin) {
            $form->addPassword('oldpassword', 'Původní heslo:')
                ->setRequired(false)
                ->addConditionOn($form['password'], $form::FILLED, true)
                ->addRule($form::REQUIRED, 'Při změně hesla je nutno uvést původní heslo.');
        } else {
            $roles = array_combine($this->permission->getRoles(), $this->permission->getRoles());
            unset($roles['guest']);
            $form->addSelect('role', 'Role:', $roles);
        }


        return $form;
    }

}
