<?php

namespace App\Presenters;

use App\Forms\ReservationFormFactory;
use App\Model\UserManager;
use Nette;
use App\Services;
use Tracy\Debugger;
use IPub\VisualPaginator\Components as VisualPaginator;


/**
 * Base presenter for all application presenters.
 */
class UsersPresenter extends BasePresenter
{

    const POSTS_PER_PAGE = 10;

    /**
     * @var UserManager
     */
    private $userManager;


    /** @persistent */
    public $sortBy = "surname";

    /** @persistent */
    public $sort = "ASC";

    public function __construct(UserManager $userManager)
    {
        $this->userManager = $userManager;
    }

    public function actionDefault()
    {
        if (!$this->user->isAllowed('User:manage')) {
            $this->flashMessage('Nemáte oprávnění prohlížet uživatele.', 'danger');
            $this->redirect('Catalog:');
        }
    }

    public function renderDefault()
    {
        $users = $this->userManager->getUsers()->order($this->sortBy . ' ' . $this->sort);

        $visualPaginator = $this['visualPaginator'];
        // Get paginator form visual paginator
        $paginator = $visualPaginator->getPaginator();
        // Define items count per one page
        $paginator->itemsPerPage = self::POSTS_PER_PAGE;
        // Define total items in list
        $paginator->itemCount = $users->count();
        // Apply limits to list
        $users->limit($paginator->itemsPerPage, $paginator->offset);

        $this->template->users = $users->fetchPairs('id');

    }


    /**
     * Create items paginator
     *
     * @return VisualPaginator\Control
     */
    protected function createComponentVisualPaginator()
    {
        // Init visual paginator
        $control = new VisualPaginator\Control;
        $control->setTemplateFile('bootstrap.latte');
        $control->disableAjax();

        return $control;
    }
}
