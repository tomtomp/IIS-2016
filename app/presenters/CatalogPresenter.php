<?php

namespace App\Presenters;

use Nette;
use App\Services;
use Tracy\Debugger;
use IPub\VisualPaginator\Components as VisualPaginator;


/**
 * Base presenter for all application presenters.
 */
class CatalogPresenter extends BasePresenter
{

    const POSTS_PER_PAGE = 10;

    /**
     * @var Services\TeaserService
     */
    private $teaserService;

    /** @persistent */
    public $filterValues = [];

    /** @persistent */
    public $sortBy = "name";

    /** @persistent */
    public $sort = "ASC";

    public function __construct(Services\TeaserService $teaserService)
    {
        $this->teaserService = $teaserService;
    }

    public function renderDefault()
    {
        $filterValues = $this->filterValues;
        $teasers = $this->teaserService->getTeasers()->order($this->sortBy . " ". $this->sort);

        if(isset($filterValues['type']) && $filterValues['type']){
            $teasers->where('pp_teaser_type_id',$filterValues['type']);
        }

        if(isset($filterValues['difficulty']) && $filterValues['difficulty']){
            $teasers->where(':pp_teaser_diff.pp_difficulty.id',$filterValues['difficulty']);
        }

        if(isset($filterValues['solved']) && $filterValues['solved'] != '?'){
            $teasers->where(':pp_loan.success',$filterValues['solved'] == '1' ? 1 : null);
        }

        // Todo null must be is available
        if(isset($filterValues['borrowed']) && $filterValues['borrowed'] != '?'){
            if($filterValues['borrowed'] == '0' ) {
                $teasers->where(':pp_loan.l_to != ? OR :pp_loan.l_to IS NULL', '1970-01-01');
            }else{
                $teasers->where(':pp_loan.l_to', '1970-01-01');
            }
        }

        $visualPaginator = $this['visualPaginator'];
        // Get paginator form visual paginator
        $paginator = $visualPaginator->getPaginator();
        // Define items count per one page
        $paginator->itemsPerPage = self::POSTS_PER_PAGE;
        // Define total items in list
        $paginator->itemCount = $teasers->count();
        // Apply limits to list
        $teasers->limit($paginator->itemsPerPage, $paginator->offset);

        $this->template->teasers = array_map(function($teaser) use ($filterValues){

            $solved = $teaser->related('pp_loan')->where('success', 1);
            if($this->user->isLoggedIn()){
                $solved->where('pp_user_id', $this->user->id);
            }

            $difficulty = array_map(function($diff){
                return $diff->pp_difficulty->value;
            },$teaser->related('pp_teaser_diff')->fetchAll());

            $borrowed = $teaser->related("pp_loan")->where('l_to','1970-01-01');

            $newTeaser = $teaser->toArray();
            $newTeaser['type_name'] = $teaser->ref('pp_teaser_type')->name;
            $newTeaser['solved'] = ($solved->count() > 0) ? 'Ano' : 'Ne';
            $newTeaser['borrowed'] = ($borrowed->count() > 0) ? 'Ano' : 'Ne';
            $newTeaser['difficulty'] = join(', ',$difficulty);
            return Nette\Utils\ArrayHash::from($newTeaser);

        },$teasers->fetchPairs('id'));

        if($this->isAjax()){
            $this->redrawControl("list");
        }
    }
    
    public function createComponentFilterForm()
    {
		$form = new \Nette\Application\UI\Form();

        $form->addMultiSelect('type', 'Typ:', $this->teaserService->getTypes())
            ->setAttribute('data-placeholder', 'Zvolte typ');

        $form->addMultiSelect('difficulty', 'Obtížnost:', $this->teaserService->getDifficultyNames())
            ->setAttribute('data-placeholder', 'Zvolte obtížnost');

        $form->addSelect('solved', 'Vyřešeno:', [
            '?' => 'Nerozhoduje',
            '1' => 'Ano',
            '0' => 'Ne',
        ]);
        $form->addSelect('borrowed', 'Zapůjčen:', [
            '?' => 'Nerozhoduje',
            '1' => 'Ano',
            '0' => 'Ne',
        ]);

        $form->onSuccess[] = function (Nette\Application\UI\Form $form, Nette\Utils\ArrayHash $values) {
            $this->filterValues = $values->getIterator()->getArrayCopy();
            if(!$this->isAjax()) {
                $this->redirect('this');
            }
        };
        $form->setDefaults($this->filterValues);

		return $form;
	}

    /**
     * Create items paginator
     *
     * @return VisualPaginator\Control
     */
    protected function createComponentVisualPaginator()
    {
        // Init visual paginator
        $control = new VisualPaginator\Control;
        $control->setTemplateFile('bootstrap.latte');
        $control->disableAjax();

        return $control;
    }
}
