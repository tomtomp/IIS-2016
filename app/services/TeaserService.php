<?php
/**
 * Created by PhpStorm.
 * User: john
 * Date: 26.10.16
 * Time: 16:41
 */

namespace App\Services;

use App\Model\UserManager;
use Nette\Database\Table\ActiveRow;
use Nette\Utils\DateTime;
use \Tracy\Debugger;

class TeaserException extends \Exception
{
    public function __construct($message, $code = 0, Exception $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }

    public function __toString()
    {
        return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
    }

    public function customFunction()
    {
        echo "A custom function for this type of exception\n";
    }
}

class TeaserService extends \Nette\Object
{
    const TABLE_TEASER = 'pp_teaser';
    const TABLE_TEASER_TYPE = 'pp_teaser_type';
    const TABLE_DIFFICULTY = 'pp_difficulty';

    const BULHARIAN_DATE = '1970-01-01';

    /**
     * @var \Nette\Database\Context
     */
    private $db;

    public function __construct(\Nette\Database\Context $connection)
    {
        $this->db = $connection;
    }

    /**
     * @return string[] Returns array of themes of Puzzles.
     */
    public function getTypes()
    {
        return $this->db->table(SELF::TABLE_TEASER_TYPE)->fetchPairs('id', 'name');
    }

    /**
     * @return string[] Returns array of themes of Puzzles.
     */
    public function getDifficultyNames()
    {
        return $this->db->table(SELF::TABLE_DIFFICULTY)->fetchPairs('id', 'value');
    }

    /**
     * @param $values array Teaser object filled with data
     * @return int Returns id of the new Teaser, or -1, if the insert failed.
     * @throws TeaserException Contains information about why the add operation failed.
     */
    public function addTeaser($values)
    {
        if (!isset($values['name'])) {
            throw new TeaserException("Invalid name");
        }

        if (!isset($values['pp_teaser_type_id'])) {
            throw new TeaserException("Invalid teaser type");
        }

        $flatValues = [];
        $flatValues['pp_teaser_type_id'] = $values['pp_teaser_type_id'];
        $flatValues['name'] = $values['name'];
        $flatValues['country'] = $values['country'];
        $flatValues['solution'] = $values['solution'];
        if ($flatValues['pp_teaser_type_id'] == TeaserType::RIDDLE) {
            $flatValues['author_name'] = $values['riddle']['author_name'];
            $flatValues['author_surname'] = $values['riddle']['author_surname'];
            $flatValues['language'] = $values['riddle']['language'];
            $flatValues['best_time'] = $values['riddle']['best_time'];
        } else if ($flatValues['pp_teaser_type_id'] == TeaserType::PUZZLE) {
            $flatValues['author_name'] = $values['puzzle']['author_name'];
            $flatValues['author_surname'] = $values['puzzle']['author_surname'];
            $flatValues['theme'] = $values['puzzle']['theme'];
            $flatValues['pieces'] = $values['puzzle']['pieces'];
        }

        $teaserRow = $this->db->table(self::TABLE_TEASER)->insert($flatValues);
        return $teaserRow->id;
    }

    /**
     * Delete teaser given by its id.
     * @param $id int Id of the teaser.
     */
    public function deleteTeaser($id)
    {
        $this->getTeasers()->get($id)->delete();
    }

    /**
     * @param $id
     * @param $values
     * @return bool
     * @throws TeaserException
     */
    public function editTeaser($id, $values)
    {
        /**
         * @var $teaser ActiveRow
         */
        $teaser = $this->getTeasers()->get($id);
        if (!$teaser) {
            throw new TeaserException('Hlavolam nenalezen.');
        }
        $flatValues = [];
        $flatValues['pp_teaser_type_id'] = $values['pp_teaser_type_id'];
        $flatValues['name'] = $values['name'];
        $flatValues['country'] = $values['country'];
        $flatValues['solution'] = $values['solution'];
        if ($flatValues['pp_teaser_type_id'] == TeaserType::RIDDLE) {
            $flatValues['author_name'] = $values['riddle']['author_name'];
            $flatValues['author_surname'] = $values['riddle']['author_surname'];
            $flatValues['language'] = $values['riddle']['language'];
            $flatValues['best_time'] = $values['riddle']['best_time'];
        } else if ($flatValues['pp_teaser_type_id'] == TeaserType::PUZZLE) {
            $flatValues['author_name'] = $values['puzzle']['author_name'];
            $flatValues['author_surname'] = $values['puzzle']['author_surname'];
            $flatValues['theme'] = $values['puzzle']['theme'];
            $flatValues['pieces'] = $values['puzzle']['pieces'];
        }
        $teaser->update($flatValues);
        return false;
    }


    /**
     * @return array|\Nette\Database\Table\IRow[]|\Nette\Database\Table\Selection
     */
    public function getTeasers()
    {
        return $this->db->table(self::TABLE_TEASER);
    }

    /**
     * @param $teaserId
     * @param $difficultyId
     * @throws TeaserException
     */
    public function deleteTeaserDiif($teaserId, $difficultyId)
    {
        $diff = $this->db->table('pp_teaser_diff')->where('pp_teaser.id', $teaserId)->where('pp_difficulty.id', $difficultyId)->fetch();
        if ($diff) {
            $diff->delete();
        }
    }

    /**
     * @param $teaserId
     * @param $values
     */
    public function editTeaserDifficulty($teaserId, $values)
    {
        $difficultyId = $values['difficultyId'];
        $value = $values['value'];
        $diff = $this->db->table('pp_teaser_diff')->where('pp_teaser.id', $teaserId)->where('pp_difficulty.id', $difficultyId)->fetch();
        if ($diff) {
            $diff->update(['value' => $value]);
        } else {
            $this->db->table('pp_teaser_diff')->insert([
                'pp_difficulty_id' => $difficultyId,
                'pp_teaser_id' => $teaserId,
                'value' => $value
            ]);
        }
    }

    /**
     * @return \Nette\Database\Table\Selection
     */
    public function getLoans()
    {
        return $this->db->table('pp_loan');
    }

    /**
     * @param $loanId
     * @param $values
     * @throws TeaserException
     */
    public function saveLoan($loanId, $values)
    {
        $loan = $this->getLoans()->get($loanId);
        if (!$loan) {
            throw new TeaserException('Rezervace nenalezena.');
        }
        $values['l_to'] = ($values['l_to']) ? DateTime::from(strtotime($values['l_to'])) : null;
        $values['l_from'] = ($values['l_from']) ? DateTime::from(strtotime($values['l_from'])) : null;

        if (!$values['l_to']) {
            $values['l_to'] = self::BULHARIAN_DATE;
            // Kontrola zadanosti
            $activeLoan = $this->getLoans()->where('pp_teaser.id = ? AND l_to = ?', $values['pp_teaser_id'], self::BULHARIAN_DATE)->fetch();
            if ($activeLoan && $activeLoan->id != $loanId) {
                throw new TeaserException('Hlavolam je již vypůjčen.');
            }
        }
        $values = array_map(function ($value) {
            return trim($value) == '' ? null : $value;
        }, (array)$values);
        $loan->update($values);
    }


    public function addLoan($values)
    {
        $values['l_to'] = ($values['l_to']) ? DateTime::from(strtotime($values['l_to'])) : null;
        $values['l_from'] = ($values['l_from']) ? DateTime::from(strtotime($values['l_from'])) : null;
        if (!$values['l_to']) {
            $values['l_to'] = self::BULHARIAN_DATE;
            // Kontrola zadanosti
            if ($this->getLoans()->where('pp_teaser.id = ? AND l_to = ?', $values['pp_teaser_id'], self::BULHARIAN_DATE)->fetch()) {
                throw new TeaserException('Hlavolam je již vypůjčen.');
            }
        }

        $values = array_map(function ($value) {
            return trim($value) == '' ? null : $value;
        }, (array)$values);
        $this->getLoans()->insert($values)->id;
    }

    public function deleteLoan($loanId)
    {
        $loan = $this->getLoans()->get($loanId);
        if ($loan) {
            $loan->delete();
        }
    }
}


/**
 * Class TeaserType
 * Contains constants for types of Teasers.
 * @package App\services
 */
class TeaserType
{
    const UNKNOWN = 0;
    const WOODEN = 1;
    const METAL = 2;
    const PLASTIC = 3;
    const TWINE = 4;
    const PUZZLE = 5;
    const RIDDLE = 6;
}
