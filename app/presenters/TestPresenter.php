<?php

namespace App\Presenters;

use Nette;
use App\Services;
use Tracy\Debugger;


/**
 * Base presenter for all application presenters.
 */
class TestPresenter extends BasePresenter
{

    /**
     * @var \Nette\Database\Context
     */
    private $db;
    private $teaserService;

    public function __construct(\Nette\Database\Context $db, Services\TeaserService $teaserService)
    {
        $this->db = $db;
        $this->teaserService = $teaserService;
    }

    public function renderDefault()
    {
        $data = "Not set";
        $data = $this->db->table('pp_teaser')->select(':pp_teaser_diff.pp_difficulty.*')->fetchAll();
        dump($data);
        exit;
    }

}
