<?php

namespace App\Presenters;

use App\Model\DuplicateNameException;
use App\Model\InvalidToken;
use App\Model\NonExistingUser;
use App\Model\UserManager;
use Nette;
use App\Forms;


class SignPresenter extends BasePresenter
{
    /** @var Forms\SignFormFactory @inject */
    public $signFactory;

    /** @persistent */
    public $backlink = '';

    /**
     * @var Nette\Mail\SendmailMailer @inject
     */
    public $emailSender;

    /**
     * @var UserManager @inject
     */
    public $userManager;


    /**
     * Sign-in form factory.
     * @return Nette\Application\UI\Form
     */
    protected function createComponentSignInForm()
    {
        $form = $this->signFactory->createIn();
        $form->onSuccess[] = function (Nette\Application\UI\Form $form, $values) {
            $this->signIn($values['email'], $values['password'], $values['remember']);
        };
        return $form;
    }


    /**
     * Sign-up form factory.
     * @return Nette\Application\UI\Form
     */
    protected function createComponentSignUpForm()
    {
        $form = $this->signFactory->createUp();
        $form->onSuccess[] = function (Nette\Application\UI\Form $form, $values) {
            try {
                $this->userManager->add($values);
            } catch (DuplicateNameException $e) {
                $form->addError('E-mail nebo rodné číslo již evidujeme.');
                return;
            }
            $this->sendWelcomeEmail($values['email']);
            $this->signIn($values['email'], $values['password']);
        };
        $form->addSubmit('register', 'Registrovat');
        return $form;
    }

    protected function createComponentSignForgotForm()
    {
        $form = $this->signFactory->createForgot();
        $form->setDefaults(['email' => $this->getParameter('email')]);
        $form->onSuccess[] = function (Nette\Application\UI\Form $form, $values) {
            $email = $values['email'];
            try {
                $token = $this->userManager->getToken($email);
            } catch (NonExistingUser $e) {
                $this->flashMessage('Uživatel s daným e-mailem nenalezen.', 'danger');
                return;
            }
            $this->sendForgotEmail($email, $token);
            $this->flashMessage('E-mail s obnovou hesla byl odeslán.');
            $this->redirect('this');
        };
        $form->addSubmit('send', 'Obnovit');
        return $form;
    }


    public function actionOut()
    {
        $this->getUser()->logout();
        $this->flashMessage('Byl jste úspěšně odhlášen');
        $this->redirect('Catalog:');

    }

    public function actionRescue($email, $token)
    {
        if (!$email || !$token) {
            $this->flashMessage('Nekompletní požadavek', 'danger');
            $this->redirect('Catalog:');
        }
        $this->backlink = null;
        try {
            $this->userManager->validateToken($email, $token);
        } catch (NonExistingUser $e) {
            $this->flashMessage('Uživatel nenalezen.', 'danger');
            $this->redirect('Sign:forgot');
        } catch (InvalidToken $e) {
            $this->flashMessage('Adresa je již neplatná. Zkuste to prosím znova.', 'danger');
            $this->redirect('Sign:forgot', ['email' => $email]);
        }
    }

    protected function createComponentSignRescueForm()
    {
        $form = $this->signFactory->createRescue();
        $form->onSuccess[] = function (Nette\Application\UI\Form $form, $values) {
            $email = $this->getParameter('email');
            $token = $this->getParameter('token');
            try {
                $this->userManager->validateToken($email, $token);
                $this->userManager->rescuePassword($email, $values['password']);
            } catch (NonExistingUser $e) {
                $this->flashMessage('Uživatel nenalezen.', 'danger');
                $this->redirect('Sign:forgot');
            } catch (InvalidToken $e) {
                $this->flashMessage('Adresa je již neplatná. Zkuste to prosím znova', 'danger');
                $this->redirect('Sign:forgot', ['email' => $email]);
            }
            $this->flashMessage('Heslo obnoveno.');
            $this->signIn($email, $values['password']);
            $this->redirect('this');
        };
        $form->addSubmit('send', 'Obnovit');
        return $form;
    }

    private function sendWelcomeEmail($email)
    {
        $emailTemplate = $this->createTemplate()->setFile(dirname(__FILE__) . '/templates/email/welcome.latte');
        $message = new Nette\Mail\Message();
        $message->setSubject('Vítáme nového člena - Databáze hlavolamů')
            ->addTo($email)
            ->addBcc('xpolas34@stud.fit.vutbr.cz')
            ->setHtmlBody($emailTemplate);
        $this->emailSender->send($message);

    }


    private function signIn($email, $password, $remember = false)
    {
        try {
            $this->user->setExpiration($remember ? '14 days' : '20 minutes');
            $this->user->login($email, $password);
        } catch
        (Nette\Security\AuthenticationException $e) {
            $this->flashMessage('Přihlašovací údaje nejsou v pořádku.', 'danger');
            return;
        }
        $this->flashMessage('Byl jste úspěšně přihlášen.');
        $this->restoreRequest($this->backlink);
        $this->redirect('Catalog:');
    }

    private function sendForgotEmail($email, $token)
    {
        $emailTemplate = $this->createTemplate()->setFile(dirname(__FILE__) . '/templates/email/forgot.latte');
        $emailTemplate->token = $token;
        $emailTemplate->email = $email;
        $message = new Nette\Mail\Message();
        $message->setSubject('Zapomenuté heslo - Databáze hlavolamů')
            ->addTo($email)
            ->addBcc('xpolas34@stud.fit.vutbr.cz')
            ->setHtmlBody($emailTemplate);
        $this->emailSender->send($message);
    }

}

