<?php

namespace App\Forms;

use App\Model\TeaserService;
use Nette;
use Nette\Application\UI\Form;


class TeaserFormFactory
{
    use Nette\SmartObject;

    /** @var FormFactory */
    private $factory;


    public function __construct(FormFactory $factory)
    {
        $this->factory = $factory;
    }

    /**
     * @return Form
     */
    public function createEdit($types)
    {

        $form = new Form;
        $form->addSelect('pp_teaser_type_id', 'Druh hlavolamu', $types);
        $form->addText('name', 'Název: *')
            ->setRequired('Zadejte prosím název.')
            ->addRule(Form::MAX_LENGTH, 'Položka může obsahovat max 30 znaků.', 30);
        $form->addText('country', 'Země původu:')
            ->setRequired(false)
            ->addRule(Form::MAX_LENGTH, 'Položka může obsahovat max 20 znaků.', 20);
        $form->addTextArea('solution', 'Řešení:')
            ->setAttribute('rows', 14)
            ->setAttribute('data-wysiwyg');

        $ridlle = $form->addContainer('riddle');
        $ridlle->addText('author_name', 'Jméno autora:')
            ->setRequired(false)
            ->addRule(Form::MAX_LENGTH, 'Položka může obsahovat max 30 znaků.', 30);
        $ridlle->addText('author_surname', 'Příjmení autora:')
            ->setRequired(false)
            ->addRule(Form::MAX_LENGTH, 'Položka může obsahovat max 30 znaků.', 30);
        $ridlle->addText('language', 'Jazyk')
            ->setRequired(false)
            ->addRule(Form::MAX_LENGTH, 'Položka může obsahovat max 30 znaků.', 30);
        $ridlle->addText('best_time', 'Čas nejlepšího řešení (s):')
            ->setRequired(false)
            ->addRule(Form::INTEGER, 'Položka musí být kladné celé číslo.')
            ->addRule(Form::MIN, 'Položka musí být kladné celé číslo.', 0);

        $puzzle = $form->addContainer('puzzle');
        $puzzle->addText('author_name', 'Jméno autora:')
            ->setRequired(false)
            ->addRule(Form::MAX_LENGTH, 'Položka může obsahovat max 30 znaků.', 30);
        $puzzle->addText('author_surname', 'Příjmení autora:')
            ->setRequired(false)
            ->addRule(Form::MAX_LENGTH, 'Položka může obsahovat max 30 znaků.', 30);
        $puzzle->addText('theme', 'Téma:')
            ->setRequired(false)
            ->addRule(Form::MAX_LENGTH, 'Položka může obsahovat max 30 znaků.', 30);
        $puzzle->addText('pieces', 'Počet dílků:')
            ->setRequired(false)
            ->addRule(Form::INTEGER, 'Položka musí být kladné celé číslo.')
            ->addRule(Form::MIN, 'Položka musí být kladné celé číslo.', 0);
        return $form;
    }

    public function createEditDifficulty($types)
    {
        $form = new Form;
        $form->addSelect('difficultyId', 'Druh', $types);
        $form->addInteger('value', 'Hodnota')
            ->setRequired(true)
            ->addRule(Form::RANGE, 'Obtížnost musí být v rozsahu 0 - 10.', [0,10]);

        return $form;
    }

}
