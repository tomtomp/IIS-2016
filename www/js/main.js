$(function () {
    $('.active-chosen select').chosen();
    $.nette.init();
    // Todo
    $.nette.ext('urlHistory', {
        success: function (a, b, c, d) {
            window.history.pushState("", "", d.url + "?" + d.data);
        }
    });
});

$(function () {
    $('select[name="pp_teaser_type_id"]').on('change', function () {
        var value = $(this).val();
        var $puzzleGroup = $('*[name^=puzzle]').closest('.form-group');
        var $riddleGroup = $('*[name^=riddle]').closest('.form-group');

        $puzzleGroup.hide();
        $riddleGroup.hide();

        if (value == 5) {
            $puzzleGroup.slideDown();
        } else if (value == 6) {
            $riddleGroup.slideDown();
        }
    }).trigger('change');

});

$(function () {
    $('a.edit-difficulty').on('click', function () {
        var diffId = $(this).attr('data-diff-id');
        var diffValue = $(this).attr('data-diff-value');
        $('#frm-editDifficultyForm-name').val(diffId).trigger("chosen:updated");
        $('#frm-editDifficultyForm-value').val(diffValue);
    })
});

$(function () {
    if ($().wysihtml5) {
        $('textarea[data-wysiwyg]').wysihtml5({
            toolbar: {
                "font-styles": true, //Font styling, e.g. h1, h2, etc. Default true
                "emphasis": true, //Italics, bold, etc. Default true
                "lists": true, //(Un)ordered lists, e.g. Bullets, Numbers. Default true
                "html": false, //Button which allows you to edit the generated HTML. Default false
                "link": false, //Button to insert a link. Default true
                "image": false, //Button to insert an image. Default true,
                "color": true, //Button to change color of font
                "blockquote": false, //Blockquote
                "size": "sm" //default: none, other options are xs, sm, lg
            }
        });
    }
});

$(function () {
    if ($().datetimepicker) {
        $('input[data-datepicker]').datetimepicker({
            locale: 'cs',
            format: 'D.M.YYYY'
        });
    }
});