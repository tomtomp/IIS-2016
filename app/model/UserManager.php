<?php

namespace App\Model;

use Nette;
use Nette\Security\Passwords;
use Tracy\Debugger;


/**
 * Users management.
 */
class UserManager implements Nette\Security\IAuthenticator
{
    use Nette\SmartObject;

    const
        TABLE_NAME = 'pp_user',
        COLUMN_ID = 'id',
        COLUMN_PASSWORD_HASH = 'password',
        COLUMN_EMAIL = 'email',
        COLUMN_ROLE = 'role',
        TOKEN_EXPIRATION_TIME = '24';


    /** @var Nette\Database\Context */
    private $database;


    public function __construct(Nette\Database\Context $database)
    {
        $this->database = $database;
    }


    /**
     * Performs an authentication.
     * @return Nette\Security\Identity
     * @throws Nette\Security\AuthenticationException
     */
    public function authenticate(array $credentials)
    {
        list($username, $password) = $credentials;

        $row = $this->database->table(self::TABLE_NAME)->where(self::COLUMN_EMAIL, $username)->fetch();

        if (!$row) {
            throw new Nette\Security\AuthenticationException('The email is incorrect.', self::IDENTITY_NOT_FOUND);

        } elseif (!Passwords::verify($password, $row[self::COLUMN_PASSWORD_HASH])) {
            throw new Nette\Security\AuthenticationException('The password is incorrect.', self::INVALID_CREDENTIAL);

        } elseif (Passwords::needsRehash($row[self::COLUMN_PASSWORD_HASH])) {
            $row->update([
                self::COLUMN_PASSWORD_HASH => Passwords::hash($password),
            ]);
        }

        $arr = $row->toArray();
        unset($arr[self::COLUMN_PASSWORD_HASH]);
        return new Nette\Security\Identity($row[self::COLUMN_ID], $row[self::COLUMN_ROLE], $arr);
    }


    /**
     * Adds new user.
     * @param array $values Array of values.
     * @return void
     * @throws DuplicateNameException
     */
    public function add($values)
    {
        try {
            $this->database->table('pp_user')->insert([
                'name' => $values['name'],
                'surname' => $values['surname'],
                'city' => $values['city'],
                'street' => $values['street'],
                'country' => $values['country'],
                'zip' => $values['zip'],
                'rc' => $values['rc'],
                self::COLUMN_PASSWORD_HASH => Passwords::hash($values['password']),
                self::COLUMN_EMAIL => $values['email'],
                self::COLUMN_ROLE => 'member'
            ]);
        } catch (Nette\Database\UniqueConstraintViolationException $e) {
            throw new DuplicateNameException;
        }
    }

    /**
     * @return Nette\Database\Table\Selection
     */
    public function getUsers()
    {
        $users = $this->database->table('pp_user');
        return $users;
    }

    /**
     * @param $id
     * @param $values
     * @throws DuplicateNameException
     * @throws NoSpareAdmin
     * @throws NonExistingUser
     */
    public function editUser($id, $values)
    {
        /**
         * @var $user Nette\Database\Table\ActiveRow
         */
        $user = $this->getUsers()->get($id);
        if (!$user) {
            throw new NonExistingUser();
        }
        if ($user->role == 'admin' && $values['role'] != 'admin' && $this->getUsers()->where('role', 'admin')->count() < 2) {
            throw new NoSpareAdmin();
        }
        if ($values['password']) {
            $values['password'] = Passwords::hash($values['password']);
        } else {
            unset($values['password']);
        }

        try {
            $user->update($values); // Weakspot
        } catch (Nette\Database\UniqueConstraintViolationException $e) {
            throw new DuplicateNameException;
        }

    }

    /**
     * @param $email
     * @return string
     * @throws NonExistingUser
     */
    public function getToken($email)
    {
        /**
         * @var $user Nette\Database\Table\ActiveRow
         */
        $user = $this->getUsers()->where('email', $email)->fetch();
        if (!$user) {
            throw new NonExistingUser();
        }
        $token = Nette\Utils\Random::generate(255);
        $user->update([
            'token' => $token,
            'token_created' => new Nette\Utils\DateTime()
        ]);
        return $token;
    }

    /**
     * @param $email
     * @param $token
     * @throws InvalidToken
     * @throws NonExistingUser
     */
    public function validateToken($email, $token)
    {

        if (strlen($token) < 255) {
            throw new InvalidToken('Token nesplňuje požadavky.');
        }
        $user = $this->getUsers()->where('email', $email)->fetch();
        if (!$user) {
            throw new NonExistingUser();
        }

        Debugger::barDump([$user->token_created, Nette\Utils\DateTime::from(strtotime('-' . self::TOKEN_EXPIRATION_TIME . ' hour'))]);
        if ($user->token_created < Nette\Utils\DateTime::from(strtotime('-24 hour'))) {
            throw new InvalidToken('Token již vypršel.');
        }

        if (!$token || $token != $user->token) {
            throw new InvalidToken('Token je nevalidní');
        }
    }

    /**
     * @param $email
     * @param $passsword
     * @throws NonExistingUser
     */
    public function rescuePassword($email, $passsword)
    {
        /**
         * @var $user Nette\Database\Table\ActiveRow
         */
        $user = $this->getUsers()->where('email', $email)->fetch();
        if (!$user) {
            throw new NonExistingUser();
        }
        $user->update([
            'token' => '',
            'password' => Passwords::hash($passsword)
        ]);
    }

    public function deleteUser($userId)
    {
        /**
         * @var $user Nette\Database\Table\ActiveRow
         */
        $user = $this->getUsers()->get($userId);
        if (!$user) {
            return;
        }
        if ($user->role == 'admin' && $this->getUsers()->where('role', 'admin')->count() < 2) {
            throw new NoSpareAdmin();
        }
        $user->delete();
    }
}


class DuplicateNameException extends \Exception
{
}

class InvalidToken extends \Exception
{
}

class NonExistingUser extends \Exception
{
}

class NoSpareAdmin extends \Exception
{
}