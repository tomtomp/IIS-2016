<?php

namespace App\Presenters;

use App\Forms\ReservationFormFactory;
use App\Model\UserManager;
use Nette;
use App\Services;
use Tracy\Debugger;
use IPub\VisualPaginator\Components as VisualPaginator;


/**
 * Base presenter for all application presenters.
 */
class ReservationsPresenter extends BasePresenter
{

    const POSTS_PER_PAGE = 10;

    /**
     * @var Services\TeaserService
     */
    private $teaserService;

    /**
     * @var UserManager
     */
    private $userManager;


    /** @persistent */
    public $sortBy = "pp_user.surname";

    /** @persistent */
    public $sort = "ASC";

    public function __construct(Services\TeaserService $teaserService, UserManager $userManager)
    {
        $this->teaserService = $teaserService;
        $this->userManager = $userManager;
    }

    public function actionAll()
    {
        if (!$this->user->isAllowed('Reservation:manage')) {
            $this->flashMessage('Nemáte oprávnění spravovat rezervace', 'danger');
            $this->redirect('Catalog:');
        }
    }

    public function actionDefault()
    {
        if (!$this->user->isLoggedIn()) {
            $this->flashMessage('Přihlastě se prosím.', 'warning');
            $this->redirect('Sign:in', ['backlink' => $this->storeRequest()]);
        }
    }

    public function renderAll()
    {
        $loansNotCompleted = $this->teaserService->getLoans()->order('l_from DESC')->where('l_to', '1970-01-01');
        $loansCompleted = $this->teaserService->getLoans()->order($this->sortBy . ' '. $this->sort)->where('l_to != ?', '1970-01-01');

        $visualPaginator = $this['visualPaginator'];
        // Get paginator form visual paginator
        $paginator = $visualPaginator->getPaginator();
        // Define items count per one page
        $paginator->itemsPerPage = self::POSTS_PER_PAGE;
        // Define total items in list
        $paginator->itemCount = $loansCompleted->count();
        // Apply limits to list
        $loansCompleted->limit($paginator->itemsPerPage, $paginator->offset);

        $this->template->loansNotCompleted = $loansNotCompleted;
        $this->template->loansCompleted = $loansCompleted;

    }

    public function renderDefault()
    {
        $loansNotCompleted = $this->teaserService->getLoans()
            ->order('l_from DESC')
            ->where('l_to', '1970-01-01')
            ->where('pp_user_id', $this->user->id);
        $loansCompleted = $this->teaserService->getLoans()
            ->order('l_from DESC')
            ->where('l_to != ?', '1970-01-01')
            ->where('pp_user_id', $this->user->id);

        $visualPaginator = $this['visualPaginator'];
        // Get paginator form visual paginator
        $paginator = $visualPaginator->getPaginator();
        // Define items count per one page
        $paginator->itemsPerPage = self::POSTS_PER_PAGE;
        // Define total items in list
        $paginator->itemCount = $loansCompleted->count();
        // Apply limits to list
        $loansCompleted->limit($paginator->itemsPerPage, $paginator->offset);

        $this->template->loansNotCompleted = $loansNotCompleted;
        $this->template->loansCompleted = $loansCompleted;

    }


    /**
     * Create items paginator
     *
     * @return VisualPaginator\Control
     */
    protected function createComponentVisualPaginator()
    {
        // Init visual paginator
        $control = new VisualPaginator\Control;
        $control->setTemplateFile('bootstrap.latte');
        $control->disableAjax();

        return $control;
    }
}
