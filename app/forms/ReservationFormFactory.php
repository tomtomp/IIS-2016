<?php

namespace App\Forms;

use App\Model\TeaserService;
use Nette;
use Nette\Application\UI\Form;


class ReservationFormFactory
{
    use Nette\SmartObject;

    /** @var FormFactory */
    private $factory;


    public function __construct(FormFactory $factory)
    {
        $this->factory = $factory;
    }

    /**
     * @return Form
     */
    public function createEdit($users, $teasers)
    {

        $form = new Form;
        $form->addSelect('pp_user_id', 'Uživatel: *', $users);
        $form->addSelect('pp_teaser_id', 'Hlavolam: *', $teasers);

        $form->addText("l_from", "Pujčeno od: *")
            ->setRequired("Zvolte prosím datum.")
            ->setAttribute("data-datepicker")
            ->setAttribute("placeholder", "d.m.rrrr")
            ->addRule($form::PATTERN, "Datum musí být ve formátu dd.mm.rrrr", "([1-9]|[12][0-9]|3[01])\.([1-9]|1[012])\.(19|20)\d\d");

        $form->addText("l_to", "Pujčeno do:")
            ->setRequired(false)
            ->setAttribute("data-datepicker")
            ->setAttribute("placeholder", "d.m.rrrr")
            ->addRule($form::PATTERN, "Datum musí být ve formátu dd.mm.rrrr", "([1-9]|[12][0-9]|3[01])\.([1-9]|1[012])\.(19|20)\d\d");

        $form->addCheckbox('success', 'Úspěšně vyřešeno:');
        $form->addInteger('rating', 'Hodnocení:')
            ->addRule(Form::RANGE, 'Hodnocení musí být v rozsahu 0 - 10.', [0,10]);

        return $form;
    }



}
